/* Modules */
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const cardRoutes = require("./routes/cardRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

/* Middlewares */
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/account", userRoutes);
app.use("/products/cards", cardRoutes);
app.use("/cart", cartRoutes);
app.use("/order", orderRoutes);

/* Connection Details */
const dbName = "capstone-02";
const dbUsername = "admin";
const dbPassword = "admin";
const dbConnectStr = `mongodb+srv://${dbUsername}:${dbPassword}@zuittbatch243.fvxpkh7.mongodb.net/${dbName}?retryWrites=true&w=majority`;
const port = 8080;

mongoose.set("strictQuery", true);
mongoose.connect(dbConnectStr, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
mongoose.connection.on("error",
    console.error.bind(console, `Connection Error`)
);
mongoose.connection.once("open", () =>
    console.log(`Successfully connected to MongoDB Atlas`)
);

app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
});
