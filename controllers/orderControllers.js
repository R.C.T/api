const Order = require("../models/Order.js");
const Cart = require("../models/Cart.js");
const Card = require("../models/Card.js");
const auth = require("../auth.js");

/* S45 Deliverables - Start */
module.exports.itemValidation = async (request, response, next) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);
    const invalidOrders = [];

    const cart = await Cart.findOne({ userId: userData.id })
        .then((result) => {
            return result;
        })
        .catch((error) => response.send(error));

    for (let i = 0; i < cart.products.length; i++) {
        await Card.findByIdAndUpdate(cart.products[i].productId)
            .then((item) => {
                let itemQuantity = cart.products[i].productQuantity;
                let stocks = item.stock;
                // Remove items from cart if item is inactive/archive
                if (!item.isActive) {
                    invalidOrders.push(item);
                    cart.products.splice(i, 1);
                    return cart.save().catch((error) => {
                        console.log(error);
                        response.send(error);
                    });
                } else {
                    // Remove items from cart if stocks are not enough
                    if (stocks < itemQuantity) {
                        invalidOrders.push(result);
                        cart.products.splice(i, 1);
                        return cart.save().catch((error) => {
                            console.log(error);
                            response.send(error);
                        });
                    } else {
                        // Deduct item quantity from the stock if stock is enough
                        item.stock -= itemQuantity;
                        return item
                            .save()
                            .then((result) => {
                                console.log(result);
                            })
                            .catch((error) => {
                                console.log(error);
                                response.send(error);
                            });
                    }
                }
            })
            .catch((error) => {
                console.log(error);
                response.send(error);
            });
    }

    console.log("Invalid orders");
    console.log(invalidOrders);

    next();
};

module.exports.checkOut = async (request, response, next) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);

    const cart = await Cart.findOne({ userId: userData.id })
        .then((result) => {
            return result;
        })
        .catch((error) => response.send(error));

    const newOrder = new Order({
        userId: userData.id,
        products: cart.products,
        totalAmount: cart.totalAmount,
    });

    // Check if cart is empty
    if (cart.products.length === 0) {
        response.send(`Cart is empty. Please add some items first.`);
        return null;
    } else {
        return newOrder
            .save()
            .then(() => {
                next();
            })
            .catch((error) => {
                console.log(error);
                response.send(error);
            });
    }
};
/* S45 Deliverables - End */

module.exports.getUserOrder = (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);

    return Order.find({ userId: userData.id })
        .then((result) => {
            response.send(result);
            return result;
        })
        .catch((error) => response.send(error));
};
