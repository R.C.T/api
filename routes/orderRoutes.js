const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers.js");
const cartController = require("../controllers/cartControllers.js");
const auth = require("../auth.js");

/* S45 Deliverables - Start */
// Check out (User only)
router.get(
    "/checkout", 
    auth.verifyToken,
    auth.isRegularUser, 
    orderController.itemValidation,
    orderController.checkOut,
    cartController.clearCart
);
/* S45 Deliverables - End */

// Get all the order of the current user (User only)
router.get(
    "/list",
    auth.verifyToken,
    auth.isRegularUser, 
    orderController.getUserOrder
)

module.exports = router;