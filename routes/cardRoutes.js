const express = require("express");
const router = express.Router();
const cardController = require("../controllers/cardControllers.js");
const auth = require("../auth.js");

/* S43 Deliverables - Start */
// Add a Card product (Admin Only)
router.post(
    "/add", 
    auth.verifyToken, 
    auth.isAdmin, 
    cardController.createCard
);

// Get all active cards
router.get(
    "/list/active", 
    cardController.getActiveCards
);


// Get all cards (Admin Only)
router.get(
    "/list", 
    auth.verifyToken, 
    auth.isAdmin, 
    cardController.getAllCards
);
/* S43 Deliverables - End */

/* S44 Deliverables - Start */
// Update a card (Admin Only)
router.patch(
    "/update",
    auth.verifyToken,
    auth.isAdmin,
    cardController.updateCard
);

// Archive/unarchive a card (Admin Only)
router.patch(
    "/archive",
    auth.verifyToken,
    auth.isAdmin,
    cardController.archiveCard
);

// Get all inactive/archive cards (Admin Only)
router.get(
    "/list/archive",
    auth.verifyToken,
    auth.isAdmin,
    cardController.getArchiveCards
);

// Search a card using card name
router.post(
    "/search", 
    cardController.searchCard
);

/* With Params */
// Get specific card using card ID
router.get(
    "/:cardId", 
    cardController.getSpecificCard
);
/* S44 Deliverables - End */

module.exports = router;
