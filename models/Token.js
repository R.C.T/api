const mongoose = require("mongoose");

const tokenSchema = new mongoose.Schema({
    tokenStr: {
        type: String,
        required: [true, "Token is required"],
    },
    dateAdded: {
        type: Date,
        immutable: true,
        default: new Date(),
    },
});

module.exports = mongoose.model("Token", tokenSchema);
