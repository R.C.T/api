const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        immutable: true,
        required: [true, "Username is required"],
    },
    products: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                immutable: true,
                required: [true, "Product ID is required"],
            },
            productName: {
                type: String,
                immutable: true,
                required: [true, "Product name is required"],
            },
            productPrice: {
                type: Number,
                immutable: true,
                required: [true, "Product price is required"],
            },
            productQuantity: {
                type: Number,
                immutable: true,
                required: [true, "Product quantity is required"],
            },
            totalProductPrice: {
                type: Number,
                immutable: true,
                required: [true, "Total product price is required"],
            },
        },
    ],
    totalAmount: {
        type: Number,
        immutable: true,
        required: [true, "Total amount is required"],
    },
    purchasedOn: {
        type: Date,
        immutable: true,
        default: new Date(),
    },
});

module.exports = mongoose.model("Order", orderSchema);
