const mongoose = require("mongoose");

const cardSchema = new mongoose.Schema({
    cardName: {
        type: String,
        required: [true, "Card name is required"],
    },
    description: {
        type: String,
        required: [true, "Description is required"],
    },
    gameName: {
        type: String,
        required: [true, "Game name is required"],
    },
    price: {
        type: Number,
        required: [true, "Price is required"],
    },
    stock: {
        type: Number,
        required: [true, "Stocks is required"],
    },
    imgSrc: {
        type: String,
        required: [true, "Image is required"]
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        immutable: true,
        default: new Date(),
    },
});

module.exports = mongoose.model("Card", cardSchema);
