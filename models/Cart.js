const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        immutable: true,
        required: [true, "Username is required"],
    },
    products: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                immutable: true,
                required: [true, "Product ID is required"],
            },
            productName: {
                type: String,
                immutable: true,
                required: [true, "Product name is required"],
            },
            productPrice: {
                type: Number,
                required: [true, "Product price is required"],
            },
            productQuantity: {
                type: Number,
                required: [true, "Product quantity is required"],
            },
            totalProductPrice: {
                type: Number,
                required: [true, "Total product price is required"],
            },
        },
    ],
    totalAmount: {
        type: Number,
        default: 0,
    },
    dateAdded: {
        type: Date,
        immutable: true,
        default: new Date(),
    },
});

module.exports = mongoose.model("Cart", cartSchema);
