const { response } = require("express");
const jwt = require("jsonwebtoken");
const secret = "capstone-02";

/* S42 Deliverables - Start */
module.exports.createAccessToken = (result) => {
    const data = {
        id: result._id,
        username: result.username,
        email: result.email,
        isAdmin: result.isAdmin,
    };

    return jwt.sign(data, secret, {});
};

module.exports.verifyToken = (request, response, next) => {
    let token = request.headers.authorization;

    if (token !== undefined) {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error) => {
            if (error) {
                // return response.send(`Invalid Token.`);
                return response.send({isValidToken: false});
            } else {
                next();
            }
        });
    } else {
        // return response.send(`Authentication failed! No token provided.`);
        return response.send({isValidToken: false});
    }
};

module.exports.getPayload = (token) => {
    if (token === undefined) {
        return null;
    } else {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error) => {
            if (error) {
                return null;
            } else {
                return jwt.decode(token, { complete: true }).payload;
            }
        });
    }
};

module.exports.isAdmin = (request, response, next) => {
    const token = request.headers.authorization;
    const userData = this.getPayload(token);

    if (!userData.isAdmin) {
        return response
            .status(403)
            // .send(`Sorry, you are not permitted to do this action`);
            .send({accessDenied: true})
    } else {
        next();
    }
};

module.exports.isRegularUser = (request, response, next) => {
    const token = request.headers.authorization;
    const userData = this.getPayload(token);

    if (userData.isAdmin) {
        return response
            .status(403)
            // .send(`Sorry, you are not permitted to do this action`);
            .send({accessDenied: true})
    } else {
        next();
    }
};
/* S42 Deliverables - End */

module.exports.createPasswordToken = (result) => {
    const data = {
        id: result._id,
        username: result.username,
        email: result.email,
        password: result.password
    };

    return jwt.sign(data, secret, {expiresIn: "1m"});
};

module.exports.decodePasswordToken = (token) => {
    if (token === undefined) {
        return null;
    } else {
        return jwt.verify(token, secret, (error, result) => {
            if (error) {
                return null;
            } else {
                console.log(result);
                return jwt.decode(token, { complete: true }).payload;
            }
        });
    }
};
